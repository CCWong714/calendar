import React from "react";
import {
  Inject,
  ScheduleComponent,
  Day,
  Week,
  WorkWeek,
  Month,
  Agenda,
  ViewsDirective,
  ViewDirective,
  EventSettingsModel,
  TimelineViews,
  TimelineMonth,
  DragAndDrop,
  Resize,
  DragEventArgs,
  ResizeEventArgs,
} from "@syncfusion/ej2-react-schedule";
import { ScrollOptions } from "@syncfusion/ej2-react-schedule";
import { DataManager, WebApiAdaptor } from "@syncfusion/ej2/data";

function App() {
  const localData = {
    dataSource: [
      {
        Id: 1,
        EndTime: new Date(2019, 0, 11, 6, 30),
        StartTime: new Date(2019, 0, 11, 4, 0),
        Subject: "Testing",
      },
    ],
  };
  // const localData = [
  //   {
  //     Id: 1,
  //     EndTime: new Date(2019, 4, 8, 6, 0),
  //     StartTime: new Date(2019, 4, 8, 7, 0),
  //     Subject: "Meditation Time",
  //     Location: "Yoga Center",
  //     // Summary: "",
  //     // IsAllDay: true,
  //     // RecurrenceRule: "FREQ=DAILY;INTERVAL=1;COUNT=10",
  //     // IsReadOnly: true,
  //     // isBlock: true,
  //   },
  //   {
  //     Id: 2,
  //     End: new Date(2019, 0, 21, 8, 30),
  //     Start: new Date(2019, 0, 21, 7, 0),
  //     Subject: "Skating Class",
  //     Location: "Tower Park",
  //     // Summary: "Meeting ",
  //     // IsReadOnly: true,
  //   },
  // ];

  const remoteData = new DataManager({
    url: "https://js.syncfusion.com/demos/ejservices/api/Schedule/LoadData",
    adaptor: new WebApiAdaptor(),
    crossDomain: true,
  });

  return (
    <>
      <ScheduleComponent
        currentView='Week'
        height='550px'
        // selectedDate={new Date(2017, 5, 5)}
        selectedDate={new Date(2019, 0, 11)}
        // eventSettings={remoteData}
        eventSettings={localData}
      >
        <ViewsDirective>
          <ViewDirective
            option='Day'
            // interval={3}
            // displayName='3 Days'
            // startHour='3:00'
            // endHour='18:00'
          ></ViewDirective>
          <ViewDirective
            option='Week'
            isSelected='true'
            showWeekNumber={true}
          ></ViewDirective>
          <ViewDirective option='Month'></ViewDirective>
          <ViewDirective option='TimelineDay'></ViewDirective>
          <ViewDirective option='TimelineMonth'></ViewDirective>
        </ViewsDirective>
        <Inject
          services={[
            Day,
            Week,
            WorkWeek,
            Month,
            Agenda,
            DragAndDrop,
            Resize,
            TimelineMonth,
            TimelineViews,
          ]}
        />
      </ScheduleComponent>
    </>
  );
}

export default App;
